package com.system.bank.loader;

import org.springframework.stereotype.Service;

@Service
public interface Loader {

    String loadString();

    int loadInt();

    Double loadDouble();
}
