package com.system.bank.loader;

import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class LoaderService implements Loader {

    private final Scanner scanner;

    public LoaderService() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String loadString() {
        return scanner.next();
    }

    @Override
    public int loadInt() {
        return scanner.nextInt();
    }

    @Override
    public Double loadDouble() {
        return scanner.nextDouble();
    }
}
