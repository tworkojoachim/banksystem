package com.system.bank.account;

import com.system.bank.transaction.Transaction;
import com.system.bank.user.client.Client;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
public class Account {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double cash;

    @Column(unique = true)
    private String accountNumber;

    @OneToOne(mappedBy = "account")
    private Client client;

    public static class AccountBuilder {

        public Double cash;
        private String accountNumber;
        private Client client;

        public AccountBuilder cash(Double cash) {
            this.cash = cash;
            return this;
        }

        public AccountBuilder accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public AccountBuilder client(Client client) {
            this.client = client;
            return this;
        }

        public Account build() {
            Account account = new Account();
            account.cash = this.cash;
            account.accountNumber = this.accountNumber;
            account.client = this.client;
            return account;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(cash, account.cash) && Objects.equals(accountNumber, account.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cash, accountNumber);
    }
}



