package com.system.bank.account;

import com.system.bank.event.EventListener;

public class AccountListener implements EventListener {

    @Override
    public void update(String event, String message) {
        System.out.println("Business notification :: EventType -> " + event + " :: Message -> " + message);
    }
}
