package com.system.bank.account;

import com.system.bank.user.client.Client;
import com.system.bank.user.client.ClientRepository;

public interface AccountService {

    void accountBalance(Client client);
    void paymentOnAccount(Client client) throws InterruptedException;
    void withdrawalFormTheAccount(Client client) throws InterruptedException;
}
