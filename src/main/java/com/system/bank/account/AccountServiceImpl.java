package com.system.bank.account;

import com.system.bank.event.EventManager;
import com.system.bank.loader.LoaderService;
import com.system.bank.transaction.Transaction;
import com.system.bank.transaction.TransactionRepository;
import com.system.bank.transaction.TransactionServiceImpl;
import com.system.bank.user.client.Client;
import com.system.bank.user.client.ClientRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AccountServiceImpl implements AccountService{

    public EventManager eventManager;
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;
    private final LoaderService loaderService;
    private final TransactionServiceImpl transactionService;
    private final TransactionRepository tRepo;

    public AccountServiceImpl(AccountRepository accountRepository, ClientRepository clientRepository, LoaderService loaderService, TransactionServiceImpl transactionService, TransactionRepository tRepo) {
        this.accountRepository = accountRepository;
        this.clientRepository = clientRepository;
        this.loaderService = loaderService;
        this.transactionService = transactionService;
        this.tRepo = tRepo;
        this.eventManager = new EventManager("accountBalance", "paymentOnAccount", "pay-offFromAccount");
    }

    @Override
    public void accountBalance(Client client) {
        Client fromDb = getClient(client.getEmail());
        eventManager.notify("accountBalance", "Odczytanie stanu konta " + client);
        System.out.println("Saldo na koncie wynosi -> " + fromDb.getAccount().getCash());
    }

    @Override
    public void paymentOnAccount(Client client) throws InterruptedException {
        Transaction transaction = transactionService.transactionStart(client);
        transaction.setTransactionType(Transaction.Type.PAYMENT);
        Client fromDb = getClient(client.getEmail());
        System.out.println("Podaj sumę jaką chcesz wpłacić");
        double cash = loaderService.loadDouble();
        eventManager.notify("paymentOnAccount", "Sprawdzanie dostepnosci..." + transaction.getState());
        Thread.sleep(2000);
        System.out.println("Czy na pewno chcesz wpłacić " + cash + " na konto " + client.getAccount().getAccountNumber());
        System.out.println("1 - Tak :: 2 - Nie");
        int d = loaderService.loadInt();
        Account account = client.getAccount();
        if(d == 1) {
            transaction.setState(Transaction.State.PROCESSED);
            eventManager.notify("paymentOnAccount", "Przetwarzanie..." + transaction.getState());
            Thread.sleep(2000);
            eventManager.notify("paymentOnAccount", "Wpłata na konto " + cash + " :: " + client);
            Thread.sleep(2000);
            account.setCash(account.getCash() + cash);
            accountRepository.save(account);
            System.out.println("Wpłacono " +cash + " na konto " + account.getAccountNumber());
            transaction.setState(Transaction.State.FINISHED);
            eventManager.notify("paymentOnAccount", "Transakcja zakonczona " + transaction.getState());
        }
        else {
            eventManager.notify("paymentOnAccount", "Odrzucono wpłate " + cash + " :: " + client);
            System.out.println("Odrzucono wpłate " +cash + " na konto " + account.getAccountNumber());
            transaction.setState(Transaction.State.CANCELED);
            eventManager.notify("paymentOnAccount", "Transakcja anulowana " + transaction.getState());
        }
        transaction.setEndTransactionTime(LocalDateTime.now());
        transaction.setAddress("Own deposit");
        transaction.setCustomerName(client.getFirstName() + " " + client.getLastName());
        transaction.setCash(cash);
        tRepo.save(transaction);
    }

    @Override
    public void withdrawalFormTheAccount(Client client) throws InterruptedException {
        Transaction transaction = transactionService.transactionStart(client);
        transaction.setTransactionType(Transaction.Type.PAY_OFF);
        Client fromDb = getClient(client.getEmail());
        System.out.println("Podaj sumę jaką chcesz wypłacić");
        double cash = loaderService.loadDouble();
        eventManager.notify("pay-offFromAccount", "Sprawdzanie dostepnosci..." + transaction.getState());
        Thread.sleep(2000);
        System.out.println("Czy na pewno chcesz wypłacić " + cash + " z konta " + client.getAccount().getAccountNumber());
        System.out.println("1 - Tak :: 2 - Nie");
        int d = loaderService.loadInt();
        Account account = client.getAccount();
        if(d == 1) {
            if(cash <= account.getCash()) {
                transaction.setState(Transaction.State.PROCESSED);
                eventManager.notify("pay-offFromAccount", "Przetwarzanie..." + transaction.getState());
                Thread.sleep(2000);
                eventManager.notify("pay-offFromAccount", "Wypłata z konta " + cash + " :: " + client);
                Thread.sleep(2000);
                account.setCash(account.getCash() - cash);
                accountRepository.save(account);
                System.out.println("Wypłacono " + cash + " z konta " + account.getAccountNumber());
                transaction.setState(Transaction.State.FINISHED);
                eventManager.notify("pay-offFromAccount", "Transakcja zakonczona " + transaction.getState());
            }
            else {
                eventManager.notify("pay-offFromAccount", "Nie wystarczajaca ilosc srodkow na konice " + cash + " :: " + client);
                System.out.println("Brak srodkow " +cash + " na koncie " + account.getAccountNumber());
                transaction.setState(Transaction.State.BLOCKED);
                eventManager.notify("pay-offFromAccount", "Transakcja zablokowana " + transaction.getState());
            }
        }
        else {
            eventManager.notify("pay-offFromAccount", "Odrzucono wypłate " + cash + " :: " + client);
            System.out.println("Odrzucono wypłate " +cash + " z konta " + account.getAccountNumber());
            transaction.setState(Transaction.State.CANCELED);
            eventManager.notify("pay-offFromAccount", "Transakcja anulowana " + transaction.getState());
        }
        transaction.setEndTransactionTime(LocalDateTime.now());
        transaction.setAddress("Own deposit");
        transaction.setCustomerName(client.getFirstName() + " " + client.getLastName());
        transaction.setCash(cash);
        tRepo.save(transaction);
    }

    private Client getClient(String email) {
        return clientRepository.getClient(email);
    }
}
