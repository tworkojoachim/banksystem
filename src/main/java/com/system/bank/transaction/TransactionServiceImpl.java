package com.system.bank.transaction;

import com.system.bank.account.Account;
import com.system.bank.account.AccountRepository;
import com.system.bank.contacts.Contacts;
import com.system.bank.contacts.ContactsRepository;
import com.system.bank.event.EventManager;
import com.system.bank.loader.LoaderService;
import com.system.bank.user.client.Client;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final LoaderService loaderService;
    private final AccountRepository aRepo;
    private final TransactionRepository tRepo;
    private final ContactsRepository coRepo;
    public EventManager eventManager;

    public TransactionServiceImpl(LoaderService loaderService, AccountRepository aRepo, TransactionRepository tRepo, ContactsRepository coRepo) {
        this.loaderService = loaderService;
        this.aRepo = aRepo;
        this.tRepo = tRepo;
        this.coRepo = coRepo;
        this.eventManager = new EventManager("cashTransfer");
    }

    @Override
    public void cashTransfer(Client client) throws InterruptedException {
        // Started transaction
        if(!checkIfTransactionPossible(client)) {

            Transaction transaction = transactionStart(client);
            transaction.setTransactionType(Transaction.Type.TRANSFER);
            processing(transaction);
            chooseCustomer(transaction, client);
            processing(transaction);
            prepare(transaction);
            processing(transaction);
        }
        else {
            eventManager.notify("cashTransfer", "Nie można rozpocząć tranaskcji, przedz zakonczeniem innej");
        }

    }

    @Override
    public void history(Client loggedUser) throws InterruptedException {
        System.out.println("Wczytywanie historii transakcji.... ");
        Thread.sleep(2000);
        List<Transaction> transactionList = tRepo.findAllByClient(loggedUser);

        for (Transaction x: transactionList) {
            print(x);
        }
    }

    private void print(Transaction t) {
        System.out.println("Transaction date :: " + t.getStartTransactionTime());
        System.out.println("Transaction type :: " + t.getTransactionType());
        System.out.println("Transaction customer :: " + t.getCustomerName());
        System.out.println("Transaction cash :: " + t.getCash());
        System.out.println("Transaction address :: " + t.getAddress());
        System.out.println("Transaction state :: " + t.getState() + "\n\n");
    }

    private void processing(Transaction transaction) throws InterruptedException {
        System.out.println("Przetwarzanie.... Status :: " + transaction.getState());
        Thread.sleep(2000);
    }

    private boolean checkIfTransactionPossible(Client client) {
        List<Transaction> transactionList = tRepo.findAllByClient(client);
        return transactionList.stream().anyMatch(t -> t.getState().equals(Transaction.State.STARTED) || t.getState().equals(Transaction.State.PROCESSED));
    }

    public Transaction transactionStart(Client client) {
        eventManager.notify("cashTransfer", "Rozpoczęcie transakcji :: Status " + Transaction.State.STARTED + " " + client);
        Transaction transaction = new Transaction();
        transaction.setStartTransactionTime(LocalDateTime.now());
        transaction.setState(Transaction.State.STARTED);
        transaction.setClient(client);
        transaction.setFromAccount(client.getAccount().getAccountNumber());
        return tRepo.save(transaction);
    }

    private void chooseCustomer(Transaction transaction, Client client)  {
        System.out.println("1 -> Nowy przelew :: 2 -> Przelew z listy odbiorców");
        int choice = loaderService.loadInt();

        if(choice == 1) {
            loadData(transaction);
        }
        else if(choice == 2) {
            loadContact(client, transaction);
        }
        else {
            blockedTransaction(transaction);
        }
    }

    private void loadData(Transaction transaction) {
        System.out.println("Podaj numer konta odbiorcy");
        transaction.setToAccount(loaderService.loadString());

        System.out.println("Podaj kwotę do przelewu");
        transaction.setCash(loaderService.loadDouble());

        System.out.println("Podaj nazwę odbiorcy");
        transaction.setCustomerName(loaderService.loadString());

        System.out.println("Podaj adres odbiorcy");
        transaction.setAddress(loaderService.loadString());
    }

    private void loadContact(Client client, Transaction transaction) {
        List<Contacts> contacts = coRepo.findAllByClient(client);

        for (int i = 0; i < contacts.size(); i++) {
            System.out.println(i+1 + " - " + contacts.get(i).getName() + " " + contacts.get(i).getAddress()+ " " + contacts.get(i).getAccountNumber());
        }
        int choose = loaderService.loadInt();
        Contacts contacts1 = contacts.get(choose-1);
        transaction.setAddress(contacts1.getAddress());
        transaction.setCustomerName(contacts1.getName());
        transaction.setToAccount(contacts1.getAccountNumber());

        System.out.println("Podaj kwotę do przelewu");
        transaction.setCash(loaderService.loadDouble());
    }

    private void prepare(Transaction transaction) throws InterruptedException {
        System.out.println("\n\n\n\n");
        System.out.println("Przelew =======================================");
        System.out.println("Nazwa odbiorcy " + transaction.getCustomerName());
        System.out.println("Adres odbiorcy " + transaction.getAddress());
        System.out.println("Kwota " + transaction.getCash());

        System.out.println(" 1 -> Zrealizuj :: 2 -> Odrzuć");
        int choose = loaderService.loadInt();

        if(choose == 1) {
            transaction.setState(Transaction.State.PROCESSED);
            tRepo.save(transaction);
            processing(transaction);
            makeTransfer(transaction, transaction.getClient());
        }
        else if(choose == 2) {
            eventManager.notify("cashTransfer", "Transakcja anulowana :: Status " + Transaction.State.CANCELED);
            transaction.setState(Transaction.State.CANCELED);
            transaction.setEndTransactionTime(LocalDateTime.now());
            tRepo.save(transaction);
        }
        else {
            blockedTransaction(transaction);
        }

    }

    private void makeTransfer(Transaction transaction, Client client) {
        Account account = client.getAccount();
        if(account.getCash() < transaction.getCash()) {
            blockedTransaction(transaction);
            eventManager.notify("cashTransfer", "Zbyt mało pieniędzy na koncie :: Status " + Transaction.State.BLOCKED);
            transaction.setEndTransactionTime(LocalDateTime.now());
            tRepo.save(transaction);
        }
        else {
            account.setCash(account.getCash() - transaction.getCash());
            aRepo.save(account);
            Account account1 = aRepo.findAccountByAccountNumber(transaction.getToAccount());
            account1.setCash(account1.getCash() + transaction.getCash());
            aRepo.save(account1);
            transaction.setEndTransactionTime(LocalDateTime.now());
            transaction.setState(Transaction.State.FINISHED);
            tRepo.save(transaction);
            eventManager.notify("cashTransfer", "Transfer zakończony powodzeniem");
        }
    }

    private void blockedTransaction(Transaction transaction) {
        eventManager.notify("cashTransfer", "Transakcja nie udana :: Status " + Transaction.State.BLOCKED);
        transaction.setState(Transaction.State.BLOCKED);
        transaction.setEndTransactionTime(LocalDateTime.now());
        tRepo.save(transaction);
    }

}

















