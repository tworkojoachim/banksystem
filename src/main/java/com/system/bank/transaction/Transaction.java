package com.system.bank.transaction;

import com.system.bank.user.client.Client;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String address;
    private String customerName;

    private LocalDateTime startTransactionTime;
    private LocalDateTime endTransactionTime;

    private String fromAccount;
    private String toAccount;

    private Double cash;
    private State state;

    private Type transactionType;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    public enum State {
        STARTED,
        PROCESSED,
        FINISHED,
        CANCELED,
        BLOCKED,
    }

    public enum Type {
        PAY_OFF,
        PAYMENT,
        TRANSFER,
    }

    public Transaction(LocalDateTime startTransactionTime, LocalDateTime endTransactionTime, String fromAccount, String toAccount, Double cash, State state, Client client) {
        this.startTransactionTime = startTransactionTime;
        this.endTransactionTime = endTransactionTime;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.cash = cash;
        this.state = state;
        this.client = client;
    }

    public static class TransactionBuilder {

        private LocalDateTime startTransactionTime;
        private LocalDateTime endTransactionTime;
        private String fromAccount;
        private String toAccount;
        private Double cash;
        private State state;
        private Client client;

        public TransactionBuilder startTransactionTime(LocalDateTime startTransactionTime) {
            this.startTransactionTime = startTransactionTime;
            return this;
        }

        public TransactionBuilder endTransactionTime(LocalDateTime endTransactionTime) {
            this.endTransactionTime = endTransactionTime;
            return this;
        }

        public TransactionBuilder fromAccount(String fromAccount) {
            this.fromAccount = fromAccount;
            return this;
        }

        public TransactionBuilder toAccount(String toAccount) {
            this.toAccount = toAccount;
            return this;
        }

        public TransactionBuilder cash(Double cash) {
            this.cash = cash;
            return this;
        }

        public TransactionBuilder state(State state) {
            this.state = state;
            return this;
        }

        public TransactionBuilder client(Client client) {
            this.client = client;
            return this;
        }

        public Transaction build() {
            return new Transaction(startTransactionTime, endTransactionTime, fromAccount, toAccount, cash, state, client);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id) && Objects.equals(startTransactionTime, that.startTransactionTime) && Objects.equals(endTransactionTime, that.endTransactionTime) && Objects.equals(fromAccount, that.fromAccount) && Objects.equals(toAccount, that.toAccount) && Objects.equals(cash, that.cash) && state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startTransactionTime, endTransactionTime, fromAccount, toAccount, cash, state);
    }
}













