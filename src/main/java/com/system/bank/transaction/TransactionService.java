package com.system.bank.transaction;

import com.system.bank.user.client.Client;

public interface TransactionService {

    public void cashTransfer(Client client) throws InterruptedException;

    void history(Client loggedUser) throws InterruptedException;
}
