package com.system.bank.transaction;

import com.system.bank.event.EventListener;

public class TransactionListener implements EventListener {

    @Override
    public void update(String event, String message) {
        System.out.println("Business notification :: EventType -> " + event + " :: Message -> " + message);
    }
}
