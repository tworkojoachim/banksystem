package com.system.bank.fascade;

import com.system.bank.account.AccountListener;
import com.system.bank.account.AccountServiceImpl;
import com.system.bank.login.LoginServiceImpl;
import com.system.bank.login.SecurityNotificationListener;
import com.system.bank.login.Session;
import com.system.bank.registration.RegistrationListener;
import com.system.bank.registration.RegistrationServiceImpl;
import com.system.bank.transaction.TransactionListener;
import com.system.bank.transaction.TransactionServiceImpl;
import com.system.bank.user.client.Client;
import org.springframework.stereotype.Service;

@Service
public class Fascade {

    private final TransactionServiceImpl transactionService;
    private final AccountServiceImpl accountService;
    private final LoginServiceImpl loginService;
    private final RegistrationServiceImpl registrationService;

    public Fascade(TransactionServiceImpl transactionService, AccountServiceImpl accountService, LoginServiceImpl loginService, RegistrationServiceImpl registrationService) {
        this.transactionService = transactionService;
        this.accountService = accountService;
        this.loginService = loginService;
        this.registrationService = registrationService;
    }

    public void cashTransfer(Client client) throws InterruptedException {
        transactionService.cashTransfer(client);
    }

    public void register() {
        registrationService.register();
    }

    public void subscribers() {
        loginService.eventManager.subscribe("login", new SecurityNotificationListener());
        loginService.eventManager.subscribe("logout", new SecurityNotificationListener());
        registrationService.eventManager.subscribe("registration", new RegistrationListener());
        accountService.eventManager.subscribe("accountBalance", new AccountListener());
        accountService.eventManager.subscribe("paymentOnAccount", new AccountListener());
        accountService.eventManager.subscribe("pay-offFromAccount", new AccountListener());
        transactionService.eventManager.subscribe("cashTransfer", new TransactionListener());
    }

    public void history(Client client) throws InterruptedException {
        transactionService.history(client);
    }

    public void accountBalance(Client client) {
        accountService.accountBalance(client);
    }

    public void paymentOnAccount(Client client) throws InterruptedException {
        accountService.paymentOnAccount(client);
    }

    public void withdrawalFormTheAccount(Client client) throws InterruptedException {
        accountService.withdrawalFormTheAccount(client);
    }

    public Session login() {
        return loginService.login();
    }

    public void logout(Session session) {
         loginService.logout(session);
    }


}
