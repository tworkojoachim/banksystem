package com.system.bank.contacts;

import com.system.bank.user.client.Client;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class Contacts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String address;
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
}
