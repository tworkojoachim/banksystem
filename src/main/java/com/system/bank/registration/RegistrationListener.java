package com.system.bank.registration;

import com.system.bank.event.EventListener;

public class RegistrationListener implements EventListener {

    @Override
    public void update(String event, String message) {
        System.out.println("Business notification :: EventType -> " + event + " :: Message -> " + message);
    }
}
