package com.system.bank.registration;

import com.system.bank.account.Account;
import com.system.bank.account.AccountRepository;
import com.system.bank.bank.BankRepository;
import com.system.bank.event.EventManager;
import com.system.bank.loader.Loader;
import com.system.bank.user.client.Client;
import com.system.bank.user.client.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    public EventManager eventManager;
    private final Loader loader;
    private final AccountRepository aRepo;
    private final ClientRepository cRepo;
    private final BankRepository bRepo;

    public RegistrationServiceImpl(Loader loader, AccountRepository aRepo, ClientRepository cRepo, BankRepository bRepo) {
        this.loader = loader;
        this.aRepo = aRepo;
        this.cRepo = cRepo;
        this.bRepo = bRepo;
        this.eventManager = new EventManager("registration");
    }

    @Override
    public void register() {
        System.out.println("W celu założenia konta uzupełnij dane");
        System.out.print("Imie: ");
        String firstName = loader.loadString();

        System.out.print("Nazwisko: ");
        String lastName = loader.loadString();

        System.out.print("Email: ");
        String email = loader.loadString();

        System.out.print("Hasło: ");
        String password = loader.loadString();

        Client client = cRepo.save(new Client.ClientBuilder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .password(password)
                .bank(bRepo.findBankByBankName("BNP Paribas"))
                .build());

        Account account = aRepo.save(new Account.AccountBuilder()
                .cash(0D)
                .accountNumber(UUID.randomUUID().toString())
                .client(client)
                .build());

        client.setAccount(account);
        cRepo.save(client);

        eventManager.notify("registration", "Rejestracja przebiegła pomyślnie -> " + client);
        System.out.println("Konto utworzone, saldo konta wynosi 0 PLN \nTwój numer konta to -> " + account.getAccountNumber());

    }
}
