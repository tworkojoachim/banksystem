package com.system.bank.registration;

import com.system.bank.user.client.Client;

public interface RegistrationService {

    public void register();
}
