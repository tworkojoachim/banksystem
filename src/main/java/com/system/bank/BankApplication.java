package com.system.bank;

import com.system.bank.fascade.Fascade;
import com.system.bank.loader.LoaderService;
import com.system.bank.login.Session;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class BankApplication {

    private static Fascade fascade;
    private static LoaderService loaderService;
    public static Session session;

    public BankApplication(Fascade fascade, LoaderService loaderService) {
        BankApplication.fascade = fascade;
        BankApplication.loaderService = loaderService;
    }


    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(BankApplication.class, args);
        fascade.subscribers();
        clearConsole();
        while(true) {
            menu();
            init();
        }
    }


    public static void init() throws InterruptedException {
        int choose = loaderService.loadInt();

        if(session == null) {
            switch (choose) {
                case 1:
                    fascade.register();
                    break;
                case 2:
                    session = fascade.login();
                    break;
                default:
                    System.out.println("Nie poprawny numer operacji");
                    break;
            }
        }
        else {
            switch (choose) {
                case 1:
                    fascade.accountBalance(session.getLoggedUser());
                    break;
                case 2:
                    fascade.paymentOnAccount(session.getLoggedUser());
                    break;
                case 3:
                    fascade.withdrawalFormTheAccount(session.getLoggedUser());
                    break;
                case 4:
                    fascade.cashTransfer(session.getLoggedUser());
                    break;
                case 5:
                    fascade.history(session.getLoggedUser());
                    break;
                case 6:
                    fascade.logout(session);
                   session = null;
                    break;
                case 7:
                    clearConsole();
                    break;
                default:
                    System.out.println("Nie poprawny numer operacji");
                    break;
            }
        }

    }



    public static void menu() {
        if(session == null) {
            System.out.println("\nWitaj w systemie bankowym");
            System.out.println("1 -> rejestracja :: 2 -> logowanie");
        }
        else {
            System.out.println("\nWitaj w swojej bankowości");
            System.out.println("1 -> stan konta :: 2 -> wpłata :: 3 -> wypłata :: 4 -> przelew :: 5 -> historia :: 6 -> wyloguj :: 7 -> wyczyść konsole " + Thread.currentThread());
        }

    }

    public static void clearConsole() {
        for (int i = 0; i < 50; i++) {
            System.out.println("\n");
        }
    }

}
