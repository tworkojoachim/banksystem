package com.system.bank.bank;

public interface BankService {

    Bank findBankByName(String bankName);
}
