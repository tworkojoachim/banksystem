package com.system.bank.bank;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BankServiceImpl implements BankService {

    private final BankRepository bRepo;

    @Override
    public Bank findBankByName(String bankName) {
        return bRepo.findBankByBankName(bankName);
    }
}
