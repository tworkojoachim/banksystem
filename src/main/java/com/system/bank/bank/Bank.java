package com.system.bank.bank;

import com.system.bank.user.client.Client;
import com.system.bank.user.employee.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bankName;

    @OneToMany(mappedBy = "bank")
    private Set<Employee> employeeSet;

    @OneToMany(mappedBy = "bank")
    private Set<Client> clientSet;

    public Bank(String bankName, Set<Employee> employeeSet, Set<Client> clientSet) {
        this.bankName = bankName;
        this.employeeSet = employeeSet;
        this.clientSet = clientSet;
    }

    public static class BankBuilder {

        private String bankName;
        private Set<Employee> employeeSet;
        private Set<Client> clientSet;

        public BankBuilder bankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        public BankBuilder employeeSet(Set<Employee> employeeSet) {
            this.employeeSet = employeeSet;
            return this;
        }

        public BankBuilder clientSet(Set<Client> clientSet) {
            this.clientSet = clientSet;
            return this;
        }

        public Bank build() {
            return new Bank(bankName, employeeSet, clientSet);
        }
    }
}
