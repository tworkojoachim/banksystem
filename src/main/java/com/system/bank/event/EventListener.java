package com.system.bank.event;

public interface EventListener {
    void update(String event, String message);
}
