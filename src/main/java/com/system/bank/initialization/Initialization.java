package com.system.bank.initialization;

import com.system.bank.account.Account;
import com.system.bank.account.AccountRepository;
import com.system.bank.bank.Bank;
import com.system.bank.bank.BankRepository;
import com.system.bank.contacts.Contacts;
import com.system.bank.contacts.ContactsRepository;
import com.system.bank.user.client.Client;
import com.system.bank.user.client.ClientRepository;
import com.system.bank.user.employee.Employee;
import com.system.bank.user.employee.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class Initialization implements CommandLineRunner {

    private final BankRepository bRepo;
    private final EmployeeRepository eRepo;
    private final ClientRepository cRepo;
    private final AccountRepository aRepo;
    private final ContactsRepository coRepo;

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Start init");
        this.createBank();
        this.createEmployee();
        this.createClients();
        System.out.println("End init");
    }

    public void createBank() {
        bRepo.save(new Bank.BankBuilder()
                .bankName("BNP Paribas")
                .clientSet(Collections.emptySet())
                .employeeSet(Collections.emptySet())
                .build());
    }

    public void createEmployee() {
        eRepo.save(new Employee.EmployeeBuilder()
                .firstName("John")
                .lastName("Smith")
                .email("smithJogn@gmail.com")
                .password("secret")
                .salary(3000D)
                .bank(bRepo.findBankByBankName("BNP Paribas"))
                .build());
    }

    public void createClients() {
        Client client = cRepo.save(new Client.ClientBuilder()
                .firstName("Joachim")
                .lastName("Tworko")
                .email("joachim@gmail.com")
                .password("secret")
                .bank(bRepo.findBankByBankName("BNP Paribas"))
                .build());

        Client client2 = cRepo.save(new Client.ClientBuilder()
                .firstName("Mark")
                .lastName("Smith")
                .email("Smith@gmail.com")
                .password("secret")
                .bank(bRepo.findBankByBankName("BNP Paribas"))
                .build());

        Account account = aRepo.save(new Account.AccountBuilder()
                .cash(0D)
                .accountNumber(UUID.randomUUID().toString())
                .client(client)
                .build());

        Account account2 = aRepo.save(new Account.AccountBuilder()
                .cash(0D)
                .accountNumber(UUID.randomUUID().toString())
                .client(client2)
                .build());

        client.setAccount(account);
        cRepo.save(client);
        client2.setAccount(account2);
        cRepo.save(client2);

        Contacts contacts = new Contacts();
        contacts.setName("Mark");
        contacts.setAddress("Legionowo 2, 34-564 Warszawa");
        contacts.setAccountNumber(account2.getAccountNumber());
        contacts.setClient(client);
        coRepo.save(contacts);

        Contacts contacts2 = new Contacts();
        contacts2.setName("Joachim");
        contacts2.setAddress("Mrągowo 4C, 23-543 Gdańsk");
        contacts2.setAccountNumber(account.getAccountNumber());
        contacts2.setClient(client2);
        coRepo.save(contacts2);
    }
}

