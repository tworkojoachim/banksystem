package com.system.bank.login;

public interface LoginService {

    public Session login();
    public void logout(Session session);
}
