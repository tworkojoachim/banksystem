package com.system.bank.login;

import com.system.bank.event.EventListener;

public class SecurityNotificationListener implements EventListener {

    @Override
    public void update(String event, String message) {
        System.out.println("Security notification :: EventType -> " + event + " :: Message -> " + message);
    }
}
