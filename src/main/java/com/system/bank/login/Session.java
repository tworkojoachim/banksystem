package com.system.bank.login;

import com.system.bank.user.client.Client;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Session {

    private Client loggedUser;
}
