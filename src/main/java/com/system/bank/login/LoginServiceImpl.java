package com.system.bank.login;

import com.system.bank.event.EventManager;
import com.system.bank.loader.LoaderService;
import com.system.bank.user.client.Client;
import com.system.bank.user.client.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginServiceImpl implements LoginService {

    public EventManager eventManager;
    private final ClientRepository cRepo;
    private final LoaderService loader;

    public LoginServiceImpl(EventManager eventManager, ClientRepository cRepo, LoaderService loader) {
        this.eventManager = eventManager;
        this.cRepo = cRepo;
        this.loader = loader;
        this.eventManager = new EventManager("login", "logout");
    }


    public Session login() {
        System.out.print("Podaj emial: ");
        String email = loader.loadString();

        System.out.print("Podaj hasło: ");
        String password = loader.loadString();

        Optional<Client> client = cRepo.findClientByEmailAndPassword(email, password);

        if(client.isPresent()) {
            eventManager.notify("login", "Zalogowano użytkownika " + client.get());
            return new Session(client.get());
        }

        else System.out.println("Nie poprawne dane logowania");
        return null;
    }

    @Override
    public void logout(Session session) {
        eventManager.notify("logout", "Wylogowano użytkownika " + session.getLoggedUser());
    }
}
