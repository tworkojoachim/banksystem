package com.system.bank.user.client;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findClientByEmailAndPassword(String email, String password);

    @Query(value = "SELECT * FROM CLIENT AS C " +
            "JOIN Account AS a " +
            "ON C.account_id = a.id " +
            "WHERE C.email =  ?1", nativeQuery = true)
    Client getClient(String email);
}
