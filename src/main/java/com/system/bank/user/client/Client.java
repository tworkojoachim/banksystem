package com.system.bank.user.client;

import com.system.bank.account.Account;
import com.system.bank.bank.Bank;
import com.system.bank.contacts.Contacts;
import com.system.bank.transaction.Transaction;
import com.system.bank.user.User;
import com.system.bank.user.employee.Employee;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Client extends User {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @OneToMany(mappedBy = "client")
    private Set<Transaction> transactionSet;

    @OneToMany(mappedBy = "client")
    private Set<Contacts> contacts;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    public Client(String email, String password, String firstName, String lastName, Account account, Bank bank, Set<Transaction> transactionSet) {
        super(email, password, firstName, lastName);
        this.account = account;
        this.bank = bank;
        this.transactionSet = transactionSet;
    }

    public static class ClientBuilder {

        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private Account account;
        private Set<Transaction> transactionSet;
        private Bank bank;

        public ClientBuilder email(String email) {
            this.email = email;
            return this;
        }

        public ClientBuilder password(String password) {
            this.password = password;
            return this;
        }

        public ClientBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public ClientBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public ClientBuilder account(Account account) {
            this.account = account;
            return this;
        }

        public ClientBuilder bank(Bank bank) {
            this.bank = bank;
            return this;
        }

        public ClientBuilder transactionSet(Set<Transaction> transactionSet) {
            this.transactionSet = transactionSet;
            return this;
        }

        public Client build() {
            return new Client(email, password, firstName, lastName, account, bank, transactionSet);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
