package com.system.bank.user.employee;

import com.system.bank.account.Account;
import com.system.bank.bank.Bank;
import com.system.bank.transaction.Transaction;
import com.system.bank.user.User;
import com.system.bank.user.client.Client;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Employee extends User {

    private Double salary;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    public Employee(String email, String password, String firstName, String lastName, Double salary, Bank bank) {
        super(email, password, firstName, lastName);
        this.salary = salary;
        this.bank = bank;
    }

    public static class EmployeeBuilder {

        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private Double salary;
        private Bank bank;

        public EmployeeBuilder email(String email) {
            this.email = email;
            return this;
        }

        public EmployeeBuilder password(String password) {
            this.password = password;
            return this;
        }

        public EmployeeBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }
        public EmployeeBuilder salary(Double salary) {
            this.salary = salary;
            return this;
        }


        public EmployeeBuilder bank(Bank bank) {
            this.bank = bank;
            return this;
        }

        public Employee build() {
            return new Employee(email, password, firstName, lastName, salary, bank);
        }
    }
}
